﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Supaplexer
{
    internal class LevelFactory
    {
        private FileObject m_file;
        private long m_levelOffset = 0; 

        public LevelFactory(ref FileObject file)
        {
            bindFile(ref file);
        }

        public LevelFactory()
        {
        }

        public void bindFile(ref FileObject file)
        {
            if (m_file != null)
                m_file.close();

            m_file = file;
        }

        public Level createLevel(int number)
        {
            Level level = new Level();

            // save the level's base in the stream
            m_levelOffset = (number - 1) * Level.LEVEL_LENGTH;

            loadLevelData(number, level);
            loadLevelName(level);
            loadLevelSpecialPorts(level);

            // set level number
            level.LevelNumber = number;

            return level;
        }

        public List<String> getLevelList()
        {
            System.Text.Encoding enc = System.Text.Encoding.ASCII;
            List<String> lvlNames = new List<String>();
            m_file.moveCursor(Level.MAP_NAME);
            int off = 1;
            do
            {
                long offset = m_file.getCursorPosition();
                String name = enc.GetString(m_file.getBetweenOffset(offset, offset + Level.MAP_NAME_LENGTH));
                offset = m_file.getCursorPosition();

                lvlNames.Add(name);
                m_file.setCursor(off * Level.LEVEL_LENGTH + Level.MAP_NAME);
                off++;
            }
            while (m_file.getCursorPosition() < m_file.getFileLength());

            return lvlNames;
        }

        private void startSection(ushort section_start)
        {
            m_file.setCursor(m_levelOffset + section_start);
        }

        private void loadLevelData(int levelNumber, Level level)
        {
            long levelOffset = (levelNumber - 1) * Level.LEVEL_LENGTH;
            m_file.setCursor(levelOffset);

            int mapOffset = 0;
            long start = m_file.getCursorPosition();

            startSection(Level.MAP_DATA);

            while (m_file.getCursorPosition() < start + Level.MAP_LENGTH)
            {
                Actor actor;
                byte actual = m_file.getNextByte();
                if (ActorMap.ByteToActor.ContainsKey(actual))
                {
                    actor = ActorMap.ByteToActor[actual];
                }
                else
                {
                    actor = new Actor("Unknown:" + Convert.ToString(actual), ImageStorage.ActorType.Unknown);
                }

                level.setActor(mapOffset++, actor);
            }

            startSection(Level.INITIAL_GRAVITY);
            level.Gravity = (m_file.getNextByte() == 1);

            startSection(Level.FREEZE_ZONKS);
            level.FreezeZonks = (m_file.getNextByte() == 2);

            startSection(Level.DEMO_INFO);
            level.DemoInfo = BitConverter.ToUInt32(m_file.getBetweenOffset(
                m_file.getCursorPosition(),
                m_file.getCursorPosition() + Level.DEMO_INFO_LENGTH), 0);

            startSection(Level.INFOTRON_COUNT);
            level.Infotrons = m_file.getNextByte();
        }

        private void loadLevelName(Level level)
        {
            System.Text.Encoding enc = System.Text.Encoding.ASCII;
            startSection(Level.MAP_NAME);
            level.Name = enc.GetString(m_file.getBetweenOffset(
                m_file.getCursorPosition(),
                m_file.getCursorPosition() + Level.MAP_NAME_LENGTH));
        }

        private void loadLevelSpecialPorts(Level level)
        {
            startSection(Level.SPECIAL_PORT_DATA_COUNT);
            level.SpecialPortCount = m_file.getNextByte();
            Debug.Assert(level.SpecialPortCount <= Level.SPECIAL_PORT_COUNT, "Reading more ports than the maximum allowed amount!");

            startSection(Level.SPECIAL_PORT_DATA);
            for (ushort port_index = 0; port_index < level.SpecialPortCount; ++port_index)
            {
                long from = m_file.getCursorPosition();
                long to = from + Level.SPECIAL_PORT_LENGTH;
                byte[] data = m_file.getBetweenOffset(from, to);
                to += Level.SPECIAL_PORT_LENGTH;
                level.addSpecialPort(new SpecialPort(data));
            }
        }
    }
}