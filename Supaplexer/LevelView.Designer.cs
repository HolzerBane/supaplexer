﻿namespace Supaplexer
{
    partial class LevelView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mapImage = new System.Windows.Forms.PictureBox();
            this.levelLoader = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.mapImage)).BeginInit();
            this.SuspendLayout();
            // 
            // mapImage
            // 
            this.mapImage.Location = new System.Drawing.Point(3, 3);
            this.mapImage.Name = "mapImage";
            this.mapImage.Size = new System.Drawing.Size(1261, 541);
            this.mapImage.TabIndex = 0;
            this.mapImage.TabStop = false;
            this.mapImage.MouseDown += new System.Windows.Forms.MouseEventHandler(this.mapImage_MouseDown);
            this.mapImage.MouseEnter += new System.EventHandler(this.mapImage_MouseEnter);
            this.mapImage.MouseLeave += new System.EventHandler(this.mapImage_MouseLeave);
            this.mapImage.MouseHover += new System.EventHandler(this.mapImage_MouseHover);
            this.mapImage.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mapImage_MouseMove);
            this.mapImage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mapImage_MouseUp);
            // 
            // levelLoader
            // 
            this.levelLoader.DoWork += new System.ComponentModel.DoWorkEventHandler(this.levelLoader_DoWork);
            this.levelLoader.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.levelLoader_ProgressChanged);
            this.levelLoader.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.levelLoader_RunWorkerCompleted);
            // 
            // LevelView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mapImage);
            this.Name = "LevelView";
            this.Size = new System.Drawing.Size(1261, 547);
            this.Click += new System.EventHandler(this.LevelView_Click);
            ((System.ComponentModel.ISupportInitialize)(this.mapImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox mapImage;
        private System.ComponentModel.BackgroundWorker levelLoader;
    }
}
