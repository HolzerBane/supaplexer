﻿using System;
using System.Windows.Forms;

namespace Supaplexer
{
    public partial class ChooseLevelDlg : Form
    {
        private int level;

        public ChooseLevelDlg()
        {
            this.Location = Cursor.Position;
            InitializeComponent();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            level = Convert.ToInt32(num_selector.Value);
            Close();
        }

        public int getSelectedLevel()
        {
            return level;
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}