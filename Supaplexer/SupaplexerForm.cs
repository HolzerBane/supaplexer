﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Supaplexer
{
    public partial class SupaplexerForm : Form
    {
        private Actor selectedPriActor;
        private Actor selectedSecActor;
        private Dictionary<int, Level> m_levelCache = new Dictionary<int, Level>();

        private Level actualLevel;
        private FileObject actualFile = new FileObject();
        private const String baseCaption = "Supaplexer";
        private bool changesMade = false;   // dirty bit
        private Timer timer = new Timer();

        private Supaplexer.ToolPanel toolBoxFlowPanel;
        private Supaplexer.LevelProperties levelProperties;
        private Supaplexer.SpecialPortProperties specialPortProperties;
        private Supaplexer.SelectedItems selectedItems;

        private Point CellAtCursor {get; set;}
        private Actor ActorAtCursor {get; set;}

private Level ActualLevel
        {
            get
            {
                return actualLevel;
            }
            set
            {
                actualLevel = value;
                levelView.bindLevel(actualLevel);
                levelView.drawLevel();
                levelProperties.bindLevel(actualLevel);
                specialPortProperties.bindLevel(actualLevel);

                actualLevel.OnSpecialPortChanged += specialPortProperties.selectGravityPort;
                actualLevel.OnSpecialPortAdded += specialPortProperties.addGravityPort;
            }
        }

        public SupaplexerForm()
        {
            InitializeComponent();
            InitializeUserControls();

            selectedItems.Enabled = false;
            mapPanel.Select();

            levelView.bindImages(actorImages);
            levelView.OnLevelViewClick += levelView_Click;
            levelView.OnLevelLoading += handleLevelLoading;
            levelView.OnLevelLoaded += handleLevelLoaded;
            levelView.OnLevelLoadingStarted += handleLevelLoadingStarted;
            levelView.OnCurrentCellChanged += handleCurrentCellChanged;
            levelProperties.levelNameEdited += onLevelNameEdited;

            statusLabel.Text = "Welcome to Supaplexer! Open a level.dat file to get started";

            // toolbox!
            foreach (Actor actor in ActorMap.ByteToActor.Values)
            {
                toolBoxFlowPanel.Controls.Add(createToolItem(actor));
            }

            resetControls();
            levelView.clearView();
            setCaption();

            mapPanel.HorizontalScroll.Minimum = 1;
            mapPanel.HorizontalScroll.Maximum = (int)Level.MAP_X;
            mapPanel.VerticalScroll.SmallChange = 20;
            mapPanel.HorizontalScroll.SmallChange = 20;
            mapPanel.VerticalScroll.LargeChange = 20;
            mapPanel.HorizontalScroll.LargeChange = 20;

            mapPanel.VerticalScroll.Minimum = 1;
            mapPanel.VerticalScroll.Maximum = (int)Level.MAP_Y;

            timer.Interval = 100;
            timer.Tick += (sender, e) => OnTick();
            timer.Start();
        }

        private void OnTick()
        {
            levelView.OnTick();
        }

        private void InitializeUserControls()
        {
            levelView = new LevelView();
            toolBoxFlowPanel = new Supaplexer.ToolPanel();
            levelProperties = new Supaplexer.LevelProperties();
            specialPortProperties = new Supaplexer.SpecialPortProperties();
            selectedItems = new Supaplexer.SelectedItems();

            toolBoxFlowPanel.Dock = DockStyle.Left;

            ToolItemPalette.Panel1.Controls.Add(selectedItems);
            ToolItemPalette.Panel2.Controls.Add(toolBoxFlowPanel);
            LevelPropertiesLayout.Controls.Add(levelProperties);
            LevelPropertiesLayout.Controls.Add(specialPortProperties);

            toolBoxFlowPanel.OnItemSelected += selectActor;
            selectedItems.OnSwap += swapSelection;
            specialPortProperties.OnSpecialPortSelected += levelView.gravityPortSelected;
            specialPortProperties.OnSpecialPortChanged += levelView.gravityPortSelected;

            levelView.bindPanel(mapPanel);
        }

        private ToolItem createToolItem(Actor actor)
        {
            try
            {
                Image img = actorImages.Images[ImageStorage.ActorImgToResEntry[actor.Image]];
                Debug.Assert(img != null, "The given image is not mapped to any resource:  " + actor);
                return new ToolItem(img, actor);
            }
            catch (System.Collections.Generic.KeyNotFoundException)
            {
                Debug.Assert(false, "The given image is not mapped to any resource:  " + actor);
                return new ToolItem();
            }
        }

        public void selectActor(Actor actor, MouseButtons button)
        {
            if (button == MouseButtons.Left)
            {
                selectedPriActor = actor;
                levelView.bindPriActor(actor);
            }
            else if (button == MouseButtons.Right)
            {
                selectedSecActor = actor;
                levelView.bindSecActor(actor);
            }

            selectedItems.Enabled = (selectedPriActor != null) && (selectedSecActor != null);

            selectedItems.setImage(levelView.getActorImage(actor), button);
        }

        public void swapSelection()
        {
            Actor oldPriActor = selectedPriActor;
            selectActor(selectedSecActor, MouseButtons.Left);
            selectActor(oldPriActor, MouseButtons.Right);
        }

        public void setCaption()
        {
            this.Text = baseCaption;
        }

        public void setCaption(String name)
        {
            setCaption();

            if (!name.Equals(""))
            {
                this.Text += " - ";
                this.Text += name;
            }
        }

        private bool save(FileObject saver)
        {
            if (!saver.isValid())
            {
                MessageBox.Show("Load a valid file first!", "Info",
                                      MessageBoxButtons.OK,
                                      MessageBoxIcon.Information);
                return false;
            }

            // write level cache to the specific locations
            foreach (var kvp in m_levelCache)
            {
                if (!saver.writeToFile(kvp.Value.getLevelAsBytes().ToArray(), (kvp.Key - 1) * Level.LEVEL_LENGTH))
                {
                    MessageBox.Show("File save error!", "Error",
                                     MessageBoxButtons.OK,
                                     MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }

        private void levelView_Click(object sender, EventArgs e)
        {
            changesMade = true;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (save(actualFile))
            {
                saveSuccessful();
                changesMade = false;
            }
        }

        private void chooseLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!actualFile.isValid())
            {
                MessageBox.Show("Load a valid file first!", "Info",
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.Information);
                return;
            }

            ChooseLevelDlg cldlg = new ChooseLevelDlg();
            if (cldlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                resetControls();

                int lvl = cldlg.getSelectedLevel();
                if (lvl >= 1 && lvl <= Level.LEVEL_COUNT)
                {
                    changeLevel(lvl);
                }
            }

        }

        private void loadLevelFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isUserDiscarding())
            {
                return;
            }

            OpenFileDialog ofdlg = new OpenFileDialog();

            if (ofdlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                string filename = ofdlg.FileName;
                if (filename.Equals(actualFile.FileName))
                {
                    actualFile.close();
                }

                if (!actualFile.open(filename))
                {
                    MessageBox.Show("File is not a valid level data file!", "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                resetControls();

                setCaption(filename);

                m_levelCache.Clear();
                levelView.clearView();

                changesMade = false;
                fillLevelList();
            }
        }

        private void fillLevelList()
        {
            int count = 1;
            foreach (String name in new LevelFactory(ref actualFile).getLevelList())
            {
                ListViewItem lvi = new ListViewItem(Convert.ToString(count++));
                lvi.SubItems.Add(name);

                levelList.Items.Add(lvi);
            }
        }

        private void changeLevel(int number)
        {
            if (!m_levelCache.ContainsKey(number))
            {
                // add level to cache
                LevelFactory lf = new LevelFactory(ref actualFile);
                m_levelCache[number] = lf.createLevel(number);
            }

            ActualLevel = m_levelCache[number];
        }

        private void resetControls()
        {
            bool fileOpened = (actualFile != null && actualFile.isValid());

            saveToolStripMenuItem.Enabled = fileOpened;
            saveAsToolStripMenuItem.Enabled = fileOpened;
            copyLevelToolStripMenuItem.Enabled = fileOpened;
            chooseLevelToolStripMenuItem.Enabled = fileOpened;

            levelList.SelectedIndices.Clear();
            levelList.Items.Clear();

            levelProperties.unbindLevel();
            specialPortProperties.unbindLevel();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdlg = new SaveFileDialog();
            if (sfdlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                String filename = sfdlg.FileName;
                FileObject newfile = new FileObject();
                if (newfile.open(filename))
                {
                    actualFile.copyTo(newfile);

                    if (save(newfile))
                    {
                        actualFile.close();
                        newfile.close();
                        // load new file
                        actualFile.open(filename);
                        setCaption(filename);
                        changesMade = false;
                        saveSuccessful();
                    }
                    else
                    {
                        MessageBox.Show("File could not be saved!", "Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("File could not be opened for edit!", "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (changesMade)
            {
                switch (MessageBox.Show("Level is not saved!\nSave changes?", "Question",
                                          MessageBoxButtons.YesNoCancel,
                                          MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        {
                            save(actualFile);
                            break;
                        }
                    case DialogResult.No:
                        {
                            if (actualFile != null && actualFile.isValid())
                            {
                                actualFile.close();
                            }
                            Application.Exit();
                            break;
                        }
                    case DialogResult.Cancel: return;
                }
            }
        }

        private void fillLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActualLevel.fillWithActor(
                ActorMap.ByteToActor.Values.First(a => a.Image == ImageStorage.ActorType.Green));

            levelView.drawLevel();
        }

        private void clearLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ActualLevel.fillWithActor(
               ActorMap.ByteToActor.Values.First(a => a.Image == ImageStorage.ActorType.Void));

            levelView.drawLevel();
        }

        private void levelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (levelList.SelectedIndices.Count == 0)
                return;

            int lvl = levelList.SelectedIndices[0];
            changeLevel(lvl + 1);
        }

        private void levelList_ItemActivate(object sender, EventArgs e)
        {
        }

        private void onLevelNameEdited(object sender, EventArgs args)
        {
            // update name list
            String newName = (sender as String); // overpowered sender for misuse
            levelList.Items[actualLevel.LevelNumber - 1].SubItems[1].Text = newName;
            levelList.Refresh();
            actualLevel.Name = newName;
            changesMade = true;
        }

        private void copyLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopyLevelDlg cldlg = new CopyLevelDlg();

            if (cldlg.ShowDialog() == DialogResult.OK)
            {
                CopyLevelDlg.Result result = cldlg.getResult();

                LevelFactory lf = new LevelFactory(ref actualFile);
                Level from = lf.createLevel(result.from);

                m_levelCache[result.to] = from;
                m_levelCache[result.to].LevelNumber = result.to;
            }
        }

        private bool isUserDiscarding()
        {
            if (changesMade)
            {
                DialogResult dlgRes = MessageBox.Show("There are unsaved changes. Continue?", "Question",
                              MessageBoxButtons.YesNo,
                              MessageBoxIcon.Question);

                return dlgRes.Equals(DialogResult.Yes) ? true : false;
            }

            return true;
        }

        private void saveSuccessful()
        {
            MessageBox.Show("Save successful", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void updateStatusText()
        {
            statusLabel.Text = "Cell at cursor: " + CellAtCursor.ToString();

            if (ActorAtCursor != null)
            {
                statusLabel.Text += " Actor: " + ActorAtCursor.ToString();

                if (ActorAtCursor.IsSpecialPort())
                {
                    var sp = ActualLevel.getGravityPort(CellAtCursor);
                    if (sp == null)
                    {
                        statusLabel.Text += " Special port info is missing!!";
                    }
                    else
                    {
                        statusLabel.Text += " turns gravity " + (sp.Gravity ? "on" : "off");
                        statusLabel.Text += ", " + (sp.FreezeEnemies ? "freezes enemies" : "unfreezes enemies");
                        statusLabel.Text += ", " + (sp.FreezeZonks ? "freezes zonks" : "unfreezes zonks");
                    }
                }
            }
        }

        private void handleCurrentCellChanged(Point cell, Actor actor)
        {
            CellAtCursor = cell;
            ActorAtCursor = actor;
            updateStatusText();
        }

        private void handleLevelLoadingStarted()
        {
            mainMenu.Enabled = false;
            levelList.Enabled = false;
            specialPortProperties.Enabled = false;
            levelProperties.Enabled = false;

            statusLabel.Text = "Loading level...";
            
            loadLevelProgressBar.Value = 0;
            loadLevelProgressBar.Show();
            loadLevelProgressBar.Maximum = actualLevel.getActors().Length;
        }

        private void handleLevelLoading(int percentage)
        {
            // I'm butchering it at this point and I dont'care:
            // this seems to be the only way to stop the progressbar from animating and lagging
            loadLevelProgressBar.Value = Math.Min(percentage + 1, loadLevelProgressBar.Maximum);
            loadLevelProgressBar.Value = percentage;
        }

        private void handleLevelLoaded()
        {
            mainMenu.Enabled = true;
            levelList.Enabled = true;
            specialPortProperties.Enabled = true;
            levelProperties.Enabled = true;

            loadLevelProgressBar.Hide();
            updateStatusText();
        }

        private void mapPanel_MouseLeave(object sender, EventArgs e)
        {
            levelView.mouseLeftParent(sender, e);
        }

        private void mapPanel_MouseEnter(object sender, EventArgs e)
        {
            levelView.mouseEnteredParent(sender, e);
        }

        private void MapLevelsTool_Panel2_MouseLeave(object sender, EventArgs e)
        {
            levelView.mouseLeftParent(sender, e);
        }

        private void MapLevelsTool_Panel2_MouseEnter(object sender, EventArgs e)
        {
            levelView.mouseEnteredParent(sender, e);
        }

    } // end of class
} // end of namespace