﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;

namespace Supaplexer
{
    public partial class LevelProperties : UserControl
    {
        private Level m_level;

        public event CancelEventHandler levelNameEdited;

        public LevelProperties()
        {
            InitializeComponent();
            unbindLevel();

            tb_name.Validating += levelNameEdited;
        }

        public void bindLevel(Level lvl)
        {
            Debug.Assert(lvl != null, "null reference passed to LevelProperties.bindLevel!");
            m_level = lvl;

            setControlEnabled(true);
            this.num_infoCount.Value = m_level.Infotrons;
            this.tb_name.Text = m_level.Name;
            this.initialGravityCheck.Checked = lvl.Gravity;
            this.freezeZonksCheck.Checked = lvl.FreezeZonks;
        }

        public void unbindLevel()
        {
            m_level = null;
            this.num_infoCount.Value = 0;
            this.tb_name.Text = string.Empty;
            this.initialGravityCheck.Checked = false;
            this.freezeZonksCheck.Checked = false;
            setControlEnabled(false);
        }

        private void setControlEnabled(bool enabled)
        {
            this.num_infoCount.Enabled = enabled;
            this.btn_max.Enabled = enabled;
            this.num_infoCount.Enabled = enabled;
            this.tb_name.Enabled = enabled;
            this.initialGravityCheck.Enabled = enabled;
            this.freezeZonksCheck.Enabled = enabled;
        }

        private void num_infoCount_ValueChanged(object sender, EventArgs e)
        {
            if (m_level != null)
            {
                m_level.Infotrons = Convert.ToInt32(this.num_infoCount.Value);
            }
        }

        private void tb_name_Validating(object sender, CancelEventArgs e)
        {
            m_level.Name = padName();
            levelNameEdited(m_level.Name, e);
        }

        private string padName()
        {
            string text = tb_name.Text;
            int tl = text.Length;
            text = text.Trim();
            // strip -s from the beginning and the end of the string
            // get the last index of continuous '-'
            //----c xzy -----
            int beginLast = 0;
            foreach (char c in text)
            {
                if (c != '-')
                    break;

                beginLast++;
            }
            int endFirst = 0;

            // get the first index of '-' at the end
            // ---- xzy c----
            for (int i = text.Length - 1; i > 0; --i)
            {
                if (text[i] != '-')
                    break;

                endFirst++;
            }

            string sub = text.Substring(beginLast, text.Length - beginLast - endFirst);
            sub = sub.Trim();
            int neededHalf = ((int)Level.MAP_NAME_LENGTH - sub.Length) / 2 - 1;
            string hyphens = new string('-', neededHalf);
            string ready = " " + hyphens + " " + sub + " " + hyphens;
            int len = ready.Length;
            // pad with '-' if needed
            for (int i = ready.Length; i < Level.MAP_NAME_LENGTH; ++i)
            {
                ready += "-";
            }

            tb_name.Text = ready;
            return ready;
        }

        private void btn_max_Click(object sender, EventArgs e)
        {
            num_infoCount.Value = m_level.getCountOfActor(ImageStorage.ActorType.Infotron);
        }

        private void initialGravityCheck_CheckedChanged(object sender, EventArgs e)
        {
            m_level.Gravity = initialGravityCheck.Checked;
        }

        private void freezeZonksCheck_CheckedChanged(object sender, EventArgs e)
        {
            m_level.FreezeZonks= freezeZonksCheck.Checked;
        }
    }
}