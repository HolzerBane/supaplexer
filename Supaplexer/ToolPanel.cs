﻿using System;
using System.Windows.Forms;

namespace Supaplexer
{
    public partial class ToolPanel : TableLayoutPanel
    {
        private ToolItem lastActive;

        public delegate void ItemSelectedHandler(Actor actor, MouseButtons buttons);
        public event ItemSelectedHandler OnItemSelected;

        public ToolPanel()
        {
            InitializeComponent();
        }

        public void unselectLastActive()
        {
            lastActive.unselect();
        }

        public void childPressed(ToolItem item, MouseButtons button)
        {
            if (lastActive != null) unselectLastActive();
            lastActive = item;

            OnItemSelected(item.getActor(), button);
            //(item.ParentForm as SupaplexerForm).selectActor(item.getActor(), button);
        }
    }
}