﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supaplexer
{
    public partial class SpecialPortProperties : UserControl
    {
        private Level m_level;

        public delegate void SpecialPortSelectedHandler(SpecialPort sp);
        public event SpecialPortSelectedHandler OnSpecialPortSelected;
        public event SpecialPortSelectedHandler OnSpecialPortChanged;

        public SpecialPortProperties()
        {
            InitializeComponent();
            setControlEnabled(false);
        }

        public void selectGravityPort(Point cell)
        {
            var portAtCell = m_level.getSpecialPorts().Where(sp => cell.X == sp.X && cell.Y == sp.Y).FirstOrDefault();
            if (portAtCell != null)
            {
                var index = m_level.getSpecialPorts().IndexOf(portAtCell);
                gravityPortList.SetSelected(index, true);
            }
        }

        public void addGravityPort(Point cell)
        {
            selectGravityPort(cell);
        }

        public void bindLevel(Level lvl)
        {
            Debug.Assert(lvl != null, "null reference passed to LevelProperties.bindLevel!");
            m_level = lvl;

            gravityPortList.DataSource = m_level.getSpecialPorts();
            gravityPortList.DisplayMember = "Name";

            setControlEnabled(true);
        }

        public void unbindLevel()
        {
            setControlEnabled(false);
            gravityPortList.Items.Clear();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var sp = getSelectedSpecialPort();
            if (sp != null)
            {
                initialiseControls(sp);
                OnSpecialPortSelected(sp);
            }
        }

        private void initialiseControls(SpecialPort sp)
        {
            enableGravityCheck.Checked = sp.Gravity;
            freezeEnemiesCheck.Checked = sp.FreezeEnemies;
            freezeZonksCheck.Checked = sp.FreezeZonks;
        }

        private void setControlEnabled(bool enabled)
        {
            gravityPortList.Enabled = enabled;
            enableGravityCheck.Enabled = enabled;
            freezeEnemiesCheck.Enabled = enabled;
            freezeZonksCheck.Enabled = enabled;
        }

        private SpecialPort getSelectedSpecialPort()
        {
            if (gravityPortList.SelectedIndices.Count == 0)
                return null;

           return m_level.getSpecialPorts()[gravityPortList.SelectedIndices[0]];
        }

        private void enableGravityCheck_CheckedChanged(object sender, EventArgs e)
        {
            var sp = getSelectedSpecialPort();
            sp.Gravity = enableGravityCheck.Checked;
            OnSpecialPortChanged(sp);
        }

        private void freezeZonksCheck_CheckedChanged(object sender, EventArgs e)
        {
            var sp = getSelectedSpecialPort();
            sp.FreezeZonks = freezeZonksCheck.Checked;
            OnSpecialPortChanged(sp);
        }

        private void freezeEnemiesCheck_CheckedChanged(object sender, EventArgs e)
        {
            var sp = getSelectedSpecialPort();
            sp.FreezeEnemies = freezeEnemiesCheck.Checked;
            OnSpecialPortChanged(sp);
        }
    }
}
