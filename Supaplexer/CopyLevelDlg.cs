﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Supaplexer
{
    public partial class CopyLevelDlg : Form
    {
        public struct Result
        {
            public int from;
            public int to;
        };

        private Result m_copyFromto;

        public KeyValuePair<int, int> CopyPair;

        public CopyLevelDlg()
        {
            this.Location = Cursor.Position;
            InitializeComponent();
        }

        public Result getResult()
        {
            return m_copyFromto;
        }

        private void btn_copy_Click(object sender, EventArgs e)
        {
            m_copyFromto.from = (int)num_from.Value;
            m_copyFromto.to = (int)num_to.Value;

            Close();
        }
    }
}