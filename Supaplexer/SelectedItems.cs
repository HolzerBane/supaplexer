﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Supaplexer
{
    public partial class SelectedItems : UserControl
    {
        public delegate void SwapHandler();
        public event SwapHandler OnSwap;

        public SelectedItems()
        {
            InitializeComponent();
        }

        public void setImage(Image image, MouseButtons buttons)
        {
            if (buttons == MouseButtons.Left)
            {
                primaryItem.Image = image;
            }
            if (buttons == MouseButtons.Right)
            {
                secondaryItem.Image = image;
            }
        }

        private void swapButton_Click(object sender, EventArgs e)
        {
            OnSwap.Invoke();
        }
    }
}
