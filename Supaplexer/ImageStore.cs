﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Supaplexer
{
    public class ImageStore
    {
        public enum ActorImage
        {
            Void,
            GreyWall,
            Green,
            Exit,
            Murphy,
            SnickSnack,
            Electron,
            Infotron,
            RedDisk,
            Bug,
            Zonk,
            BrownDisk,
            YellowDisk,
            Terminal,
            ChipLeft,
            ChipRight,
            ChipTop,
            ChipBottom,
            Chip,
            PortEast,
            PortWest,
            PortNorth,
            PortSouth,
            DiodeRed,
            DiodeBlue,
            DiodeGreen,
            DangerWall,
            ResistorWall,

            Unknown
        }

        public static Dictionary<ActorImage, string> ActorImgToResEntry = new Dictionary<ActorImage, string>()
        {
            { ActorImage.Void, "s_void.jpg"},
            { ActorImage.GreyWall, "s_wallg.jpg"},
            { ActorImage.Exit, "s_exit.jpg"},
            { ActorImage.Murphy, "s_start.jpg"},
            { ActorImage.Infotron, "s_info.jpg"},
            { ActorImage.Green, "s_green.jpg"},
            { ActorImage.Unknown, "s_unknown.jpg"},
            { ActorImage.Zonk, "s_rock.jpg"},
            { ActorImage.Bug, "s_bug.jpg"},

            { ActorImage.Chip, "s_chip.jpg"},
            { ActorImage.ChipLeft, "s_chipl.jpg"},
            { ActorImage.ChipRight, "s_chipr.jpg"},
            { ActorImage.ChipTop, "s_chipt.jpg"},
            { ActorImage.ChipBottom, "s_chipb.jpg"},

            { ActorImage.DiodeBlue, "s_diodeb.jpg"},
            { ActorImage.DiodeGreen, "s_diodeg.jpg"},
            { ActorImage.DiodeRed, "s_dioder.jpg"},
            { ActorImage.ResistorWall, "s_resistor.jpg"},
            { ActorImage.BrownDisk, "s_diskb.jpg"},
            { ActorImage.YellowDisk, "s_disky.jpg"},
            { ActorImage.RedDisk, "s_diskr.jpg"},

            { ActorImage.SnickSnack, "s_snick.jpg"},
            { ActorImage.Terminal,  "s_terminal.jpg"},

            { ActorImage.PortEast, "s_portE.jpg"},
            { ActorImage.PortWest, "s_portW.jpg"},
        };

        // Categories
        public static List<ActorImage> StartExit = new List<ActorImage>()
        {
            ActorImage.Murphy,
            ActorImage.Exit
        };

        public static List<ActorImage> Hazards = new List<ActorImage>()
        {
            ActorImage.SnickSnack,
            ActorImage.Electron,
            ActorImage.Bug,
            ActorImage.Zonk,
            ActorImage.BrownDisk
        };

        public static List<ActorImage> Walls = new List<ActorImage>()
        {
            ActorImage.Void,
            ActorImage.GreyWall,
            ActorImage.Green
        };

        public static List<ActorImage> Collectibles = new List<ActorImage>()
        {
            ActorImage.Infotron,
            ActorImage.RedDisk,
            ActorImage.Green
        };
    }
}
