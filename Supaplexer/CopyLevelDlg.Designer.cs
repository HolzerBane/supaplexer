﻿namespace Supaplexer
{
    partial class CopyLevelDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.num_from = new System.Windows.Forms.NumericUpDown();
            this.num_to = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_copy = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.num_from)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_to)).BeginInit();
            this.SuspendLayout();
            // 
            // num_from
            // 
            this.num_from.Location = new System.Drawing.Point(51, 7);
            this.num_from.Maximum = new decimal(new int[] {
            111,
            0,
            0,
            0});
            this.num_from.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_from.Name = "num_from";
            this.num_from.Size = new System.Drawing.Size(58, 20);
            this.num_from.TabIndex = 0;
            this.num_from.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // num_to
            // 
            this.num_to.Location = new System.Drawing.Point(166, 7);
            this.num_to.Maximum = new decimal(new int[] {
            111,
            0,
            0,
            0});
            this.num_to.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num_to.Name = "num_to";
            this.num_to.Size = new System.Drawing.Size(58, 20);
            this.num_to.TabIndex = 1;
            this.num_to.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "From:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(137, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "To:";
            // 
            // btn_copy
            // 
            this.btn_copy.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_copy.Location = new System.Drawing.Point(68, 33);
            this.btn_copy.Name = "btn_copy";
            this.btn_copy.Size = new System.Drawing.Size(75, 23);
            this.btn_copy.TabIndex = 4;
            this.btn_copy.Text = "Copy";
            this.btn_copy.UseVisualStyleBackColor = true;
            this.btn_copy.Click += new System.EventHandler(this.btn_copy_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Location = new System.Drawing.Point(149, 33);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_cancel.TabIndex = 5;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseVisualStyleBackColor = true;
            // 
            // CopyLevelDlg
            // 
            this.AcceptButton = this.btn_copy;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btn_cancel;
            this.ClientSize = new System.Drawing.Size(236, 69);
            this.ControlBox = false;
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_copy);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.num_to);
            this.Controls.Add(this.num_from);
            this.Name = "CopyLevelDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Copy Level";
            ((System.ComponentModel.ISupportInitialize)(this.num_from)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num_to)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown num_from;
        private System.Windows.Forms.NumericUpDown num_to;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_copy;
        private System.Windows.Forms.Button btn_cancel;
    }
}