﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

namespace Supaplexer
{
    [System.ComponentModel.DefaultBindingProperty("LevelData")]
    public class Level
    {
        public static ushort MAP_X = 60;
        public static ushort MAP_Y = 24;

        public static ushort MAP_DATA = 0;
        public static ushort MAP_LENGTH = 1440;
        public static ushort INITIAL_GRAVITY = 1444;
        public static ushort VERSION = 1445;
        public static ushort MAP_NAME = 1446;
        public static ushort MAP_NAME_LENGTH = 23;
        public static ushort FREEZE_ZONKS = 1469;
        public static ushort INFOTRON_COUNT = 1470;
        public static ushort INFOTRON_LENGTH = 1;
        public static ushort SPECIAL_PORT_DATA_COUNT = 1471;
        public static ushort SPECIAL_PORT_DATA = 1472;
        public static ushort SPECIAL_PORT_DATA_LENGTH = 60;
        public static ushort DEMO_INFO = 1532;
        public static ushort DEMO_INFO_LENGTH = 4;
        public static ushort LEVEL_LENGTH = 1536;


        public static ushort SPECIAL_PORT_COORDINATES = 0;
        public static ushort SPECIAL_PORT_GRAVITY = 2;
        public static ushort SPECIAL_PORT_FREEZE_ZONKS = 3;
        public static ushort SPECIAL_PORT_FREEZE_ENEMISES = 4;
        public static ushort SPECIAL_PORT_LENGTH = 6;


        public static int LEVEL_COUNT = 111;
        public static int SPECIAL_PORT_COUNT = 10;


        private String m_name;
        private Actor[] m_level = new Actor[MAP_LENGTH];
        private BindingList<SpecialPort> m_specialPorts = new BindingList<SpecialPort>();
        private int m_specialPortIndex;
        private int m_infoCount;
        private int m_number;

        public delegate void handleSpecialPortChanged(Point cell);
        public event handleSpecialPortChanged OnSpecialPortChanged;
        public event handleSpecialPortChanged OnSpecialPortAdded;

        public int LevelNumber
        {
            get { return m_number; }
            set { m_number = value; }
        }

        public int Infotrons
        {
            get { return m_infoCount; }
            set { m_infoCount = value; }
        }

        public String Name
        {
            set { m_name = value; }
            get { return m_name; }
        }

        public bool Gravity { get; set; }

        public bool FreezeZonks { get; set; }

        public ushort Version { get; set; }

        public uint DemoInfo { get; set; }

        public ushort SpecialPortCount { get; set; }

        public Actor[] getActors()
        {
            return m_level;
        }

        public void setActor(int x, int y, Actor actor)
        {
            Debug.Assert((x < MAP_X && x > 0), "X coordinate out of range!");
            Debug.Assert((y < MAP_Y && y > 0), "Y coordinate out of range!");
            Debug.Assert((x + y * MAP_X) < MAP_LENGTH, "Index out of range!");

            m_level[x + y * MAP_X] = actor;
        }

        public void setActor(int offset, Actor actor)
        {
            Debug.Assert(offset < MAP_LENGTH, "Index out of range!");
            m_level[offset] = actor;
        }

        public void addSpecialPort(SpecialPort port)
        {
            m_specialPorts.Add(port);
            m_specialPortIndex = (m_specialPorts.Count % SPECIAL_PORT_COUNT);
            OnSpecialPortAdded?.Invoke(new Point(port.X, port.Y));
        }

        public void addSpecialPort(Point cell)
        {
            if (m_specialPorts.Count == SPECIAL_PORT_COUNT)
            {
                // limiting the amount, reusing oldest port
                m_specialPorts[m_specialPortIndex] = new SpecialPort(cell);
            }
            else
            {
                // fill up gravity ports
                m_specialPorts.Add(new SpecialPort(cell));
            }

            m_specialPortIndex = m_specialPortIndex++ % SPECIAL_PORT_COUNT;
            OnSpecialPortAdded?.Invoke(cell);
        }

        public void removeSpecialPort(Point cell)
        {
            var index = SpecialPort.indexFromPosition((ushort)cell.X, (ushort)cell.Y);

            if (index < m_specialPortIndex)
            {
                m_specialPortIndex--;
            }

            var gpToRemove = m_specialPorts.Where(sp => sp.Index == index).FirstOrDefault();
            if (gpToRemove != null)
            {
                m_specialPorts.Remove(gpToRemove);
                OnSpecialPortChanged?.Invoke(cell);
            }
        }

        public void setGravity(SpecialPort sp, bool gravityState)
        {
            sp.Gravity = gravityState;
            OnSpecialPortChanged?.Invoke(new Point(sp.X, sp.Y));
        }

        public BindingList<SpecialPort> getSpecialPorts()
        {
            return m_specialPorts;
        }

        public void fillWithActor(Actor actor)
        {
            for (int x = 1; x < MAP_X - 1; ++x)
                for (int y = 1; y < MAP_Y - 1; ++y)
                {
                    m_level[y * MAP_X + x] = actor;
                }
        }

        public Actor getActor(int x, int y)
        {
            Debug.Assert(x < MAP_X, "X coordinate out of range!");
            Debug.Assert(y < MAP_Y, "Y coordinate out of range!");
            Debug.Assert((x + y * MAP_X) < MAP_LENGTH, "Index out of range!");

            return m_level[x + y * MAP_X];
        }

        public int getCountOfActor(ImageStorage.ActorType actor)
        {
            return m_level.Where(a => a.Image == actor).Count();
        }

        public SpecialPort getGravityPort(Point cell)
        {
            var index = SpecialPort.indexFromPosition((ushort)cell.X, (ushort)cell.Y);
            return m_specialPorts.Where(sp => sp.Index == index).FirstOrDefault();
        }

        public int getSpecialPortCount()
        {
            return m_specialPorts.Count;
        }

        public IEnumerable<byte> getLevelAsBytes()
        {
            // start with the map
            List<byte> bytes = new List<byte>();
            int debugInt = 0;
            foreach (Actor a in m_level)
            {
                bytes.Add(ActorMap.ActorToByte(a));
                debugInt++;
            }

            // write level data
            byte[] dummy = new byte[4];
            bytes.AddRange(dummy);

            // write gravity
            bytes.Add((byte)(Gravity ? 0x01 : 0x00));

            // write name string
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            bytes.AddRange(encoding.GetBytes(Name));

            // write freeze zonks
            bytes.Add((byte)(FreezeZonks ? 0x02 : 0x00));

            // write info count
            bytes.Add(Convert.ToByte(Infotrons));
            bytes.Add(0x00);

            // write gravity port count
            bytes.Add((byte)(m_specialPorts.Count));

            // write special port data
            int specialPortCount = SPECIAL_PORT_COUNT;
            foreach (SpecialPort sp in m_specialPorts)
            {
                specialPortCount--;
                bytes.AddRange(sp.toBytes());
            }

            byte[] dummyPort = new byte[6];

            while (specialPortCount--> 0)
            {
                bytes.AddRange(dummyPort);
            }

            // todo: this is actually used by the SpeedFix
            bytes.AddRange(dummy);


            //write level data (unused?)
            //bytes.AddRange(m_data);

            Debug.Assert(bytes.Count == LEVEL_LENGTH, "Level is not of a standard supaplex level size!");
            return bytes;
        }
    }
}