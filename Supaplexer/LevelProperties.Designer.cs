﻿namespace Supaplexer
{
    partial class LevelProperties
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.num_infoCount = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.freezeZonksCheck = new System.Windows.Forms.CheckBox();
            this.initialGravityCheck = new System.Windows.Forms.CheckBox();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_max = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.num_infoCount)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Infotrons to collect";
            // 
            // num_infoCount
            // 
            this.num_infoCount.Location = new System.Drawing.Point(108, 57);
            this.num_infoCount.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.num_infoCount.Name = "num_infoCount";
            this.num_infoCount.Size = new System.Drawing.Size(42, 20);
            this.num_infoCount.TabIndex = 1;
            this.num_infoCount.ValueChanged += new System.EventHandler(this.num_infoCount_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.freezeZonksCheck);
            this.groupBox1.Controls.Add(this.initialGravityCheck);
            this.groupBox1.Controls.Add(this.tb_name);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btn_max);
            this.groupBox1.Controls.Add(this.num_infoCount);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(209, 142);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Level Properties";
            // 
            // freezeZonksCheck
            // 
            this.freezeZonksCheck.AutoSize = true;
            this.freezeZonksCheck.Location = new System.Drawing.Point(9, 115);
            this.freezeZonksCheck.Name = "freezeZonksCheck";
            this.freezeZonksCheck.Size = new System.Drawing.Size(91, 17);
            this.freezeZonksCheck.TabIndex = 6;
            this.freezeZonksCheck.Text = "Freeze Zonks";
            this.freezeZonksCheck.UseVisualStyleBackColor = true;
            this.freezeZonksCheck.CheckedChanged += new System.EventHandler(this.freezeZonksCheck_CheckedChanged);
            // 
            // initialGravityCheck
            // 
            this.initialGravityCheck.AutoSize = true;
            this.initialGravityCheck.Location = new System.Drawing.Point(9, 92);
            this.initialGravityCheck.Name = "initialGravityCheck";
            this.initialGravityCheck.Size = new System.Drawing.Size(84, 17);
            this.initialGravityCheck.TabIndex = 5;
            this.initialGravityCheck.Text = "Initial gravity";
            this.initialGravityCheck.UseVisualStyleBackColor = true;
            this.initialGravityCheck.CheckedChanged += new System.EventHandler(this.initialGravityCheck_CheckedChanged);
            // 
            // tb_name
            // 
            this.tb_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tb_name.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_name.Location = new System.Drawing.Point(9, 32);
            this.tb_name.MaxLength = 24;
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(191, 19);
            this.tb_name.TabIndex = 4;
            this.tb_name.Validating += new System.ComponentModel.CancelEventHandler(this.tb_name_Validating);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Name";
            // 
            // btn_max
            // 
            this.btn_max.Location = new System.Drawing.Point(153, 57);
            this.btn_max.Name = "btn_max";
            this.btn_max.Size = new System.Drawing.Size(47, 20);
            this.btn_max.TabIndex = 2;
            this.btn_max.Text = "Max";
            this.btn_max.UseVisualStyleBackColor = true;
            this.btn_max.Click += new System.EventHandler(this.btn_max_Click);
            // 
            // LevelProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "LevelProperties";
            this.Size = new System.Drawing.Size(212, 142);
            ((System.ComponentModel.ISupportInitialize)(this.num_infoCount)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown num_infoCount;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_max;
        private System.Windows.Forms.CheckBox freezeZonksCheck;
        private System.Windows.Forms.CheckBox initialGravityCheck;
    }
}
