﻿namespace Supaplexer
{
    partial class ToolItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolText = new System.Windows.Forms.Label();
            this.toolImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.toolImage)).BeginInit();
            this.SuspendLayout();
            // 
            // toolText
            // 
            this.toolText.Location = new System.Drawing.Point(1, 38);
            this.toolText.MaximumSize = new System.Drawing.Size(120, 60);
            this.toolText.MinimumSize = new System.Drawing.Size(60, 30);
            this.toolText.Name = "toolText";
            this.toolText.Size = new System.Drawing.Size(60, 30);
            this.toolText.TabIndex = 1;
            this.toolText.Text = "label1";
            this.toolText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolText.Click += new System.EventHandler(this.ToolItem_Click);
            // 
            // toolImage
            // 
            this.toolImage.Location = new System.Drawing.Point(15, 3);
            this.toolImage.Name = "toolImage";
            this.toolImage.Size = new System.Drawing.Size(32, 32);
            this.toolImage.TabIndex = 0;
            this.toolImage.TabStop = false;
            this.toolImage.Click += new System.EventHandler(this.ToolItem_Click);
            // 
            // ToolItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolText);
            this.Controls.Add(this.toolImage);
            this.Name = "ToolItem";
            this.Size = new System.Drawing.Size(64, 72);
            this.Click += new System.EventHandler(this.ToolItem_Click);
            ((System.ComponentModel.ISupportInitialize)(this.toolImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox toolImage;
        private System.Windows.Forms.Label toolText;
    }
}
