﻿using System;

//comment
namespace Supaplexer
{
    public class Actor
    {
        public Actor(String name, ImageStorage.ActorType img)
        {
            m_name = name;
            m_image = img;
        }

        public String Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        public ImageStorage.ActorType Image
        {
            get { return m_image; }
            set { m_image = value; }
        }

        public bool IsSpecialPort() 
        {
            return
                   m_image == ImageStorage.ActorType.GravEast
                || m_image == ImageStorage.ActorType.GravWest
                || m_image == ImageStorage.ActorType.GravNorth
                || m_image == ImageStorage.ActorType.GravSouth
                || m_image == ImageStorage.ActorType.Grav4Way
                || m_image == ImageStorage.ActorType.GravHorizontal
                || m_image == ImageStorage.ActorType.GravVertical;
        }

        private String m_name;
        private ImageStorage.ActorType m_image;

        public override string ToString()
        {
            return m_name;
        }
    }
}