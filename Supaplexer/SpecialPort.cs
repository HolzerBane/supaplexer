﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supaplexer
{
    public class SpecialPort
    {
        public ushort X { get; private set; }
        public ushort Y { get; private set; }
        public ushort Index { get; private set; }

        public bool Gravity { get; set; }
        public bool FreezeZonks { get; set; }
        public bool FreezeEnemies { get; set; }

        public string Name => "[" + X.ToString() + ", " + Y.ToString() + "]";

        public Point Cell => new Point(X, Y);

        public SpecialPort(Point cell)
        {
            X = (ushort)(cell.X);
            Y = (ushort)(cell.Y);
            Index = indexFromPosition(X, Y);

            Gravity = false;
            FreezeZonks = false;
            FreezeEnemies = false;
        }

        public SpecialPort(Byte[] bytes)
        {
            byte[] index_bytes = { bytes[1], bytes[0] };
            Index = BitConverter.ToUInt16(index_bytes, 0);
            var halfIndex = Index / 2;
            X = (ushort)(halfIndex % Level.MAP_X);
            Y = (ushort)(halfIndex / Level.MAP_X);

           Gravity = (bytes[2] == 1);
           FreezeZonks = (bytes[3] == 2);
           FreezeEnemies = (bytes[4] == 1);
        }

        public void setPosition(ushort x, ushort y)
        {
            X = x;
            Y = y;
            Index = indexFromPosition(x, y);
        }

        public static ushort indexFromPosition(ushort x, ushort y)
        {
            return (ushort)(2 * (x + y * 60));
        }

        public static (ushort, ushort) positionFromIndex(ushort index)
        {
            var halfIndex = index / 2;
            ushort x = (ushort)(halfIndex % Level.MAP_X);
            ushort y = (ushort)(halfIndex / Level.MAP_X);

            return (x, y);
        }

        public byte[] toBytes()
        {
            byte[] data = new byte[Level.SPECIAL_PORT_LENGTH];
            Buffer.BlockCopy(BitConverter.GetBytes(Index), 0, data, 0, 2);

            data[2] = (byte)(Gravity ? 1 : 0);
            data[3] = (byte)(FreezeZonks ? 2 : 0);
            data[4] = (byte)(FreezeEnemies ? 2 : 0);
            data[5] = (byte)(0);

            return data;
        }
    }
}
