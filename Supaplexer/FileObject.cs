﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Supaplexer
{
    /// <summary>
    /// Manages the file stream of a LEVELS.DAT file
    /// </summary>
    internal class FileObject
    {
        public static long LEVEL_SIZE = 170496;

        private FileStream m_stream;
        public string FileName { get; set; } 

        public FileObject()
        {
        }

        public FileObject(FileStream fs)
        {
            m_stream = fs;
        }

        public void copyTo(FileObject fs)
        {
            setCursor(0);
            m_stream.CopyTo(fs.getStream());
        }

        public FileStream getStream()
        {
            return m_stream;
        }

        public long seekByte(byte pattern)
        {
            long pos = -1;

            setCursor(0);
            while (m_stream.Position != m_stream.Length)
            {
                byte actual = (byte)m_stream.ReadByte();
                if (actual == pattern)
                {
                    moveCursor(-1);
                    pos = m_stream.Position;
                    break;
                }
            }

            return pos;
        }

        public byte[] getBetweenSymbol(byte start, byte end)
        {
            bool firstfound = false;
            List<byte> bytes = new List<byte>();

            while (m_stream.Position != m_stream.Length)
            {
                byte actual = (byte)m_stream.ReadByte();
                // skip first
                if (firstfound)
                {
                    if (actual == end) break;
                    bytes.Add(actual);
                }

                if (actual == start) { firstfound = true; }
            }

            return bytes.ToArray();
        }

        public byte[] getBetweenOffset(long start, long end)
        {
            setCursor(start);

            List<byte> bytes = new List<byte>();

            while (m_stream.Position != m_stream.Length)
            {
                if (m_stream.Position == end) break;
                bytes.Add((byte)m_stream.ReadByte());
            }

            return bytes.ToArray();
        }

        public bool writeToFile(byte[] data, long from)
        {
            try
            {
                setCursor(from);

                foreach (byte b in data)
                {
                    m_stream.WriteByte(b);
                }
                m_stream.Flush();
            }
            catch (System.IO.IOException)
            {
                return false;
            }

            return true;
        }

        public byte getNextByte()
        {
            if (m_stream.Position == m_stream.Length)
                return 0xFF;

            return (byte)m_stream.ReadByte();
        }

        public long getFileLength()
        {
            return m_stream.Length;
        }

        public long setCursor(long offset)
        {
            return m_stream.Seek(offset, SeekOrigin.Begin);
        }

        public long moveCursor(long offset)
        {
            return m_stream.Seek(offset, SeekOrigin.Current);
        }

        public long getCursorPosition()
        {
            return m_stream.Position;
        }

        public bool open(String path)
        {
            FileName = path;

            try
            {
                FileStream fs = new FileStream(path, FileMode.OpenOrCreate);

                if (fs == null)
                {
                    return false;
                }

                if (m_stream != null)
                {
                    close();
                }

                m_stream = fs;
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }

        public void close()
        {
            try
            {
                if (m_stream != null)
                {
                    m_stream.Flush();
                    m_stream.Close();
                }
            }
            catch (ObjectDisposedException)
            {
                // it is already freed
                return;
            }
        }

        public bool isValid()
        {
            return isStreamOk(m_stream);
        }

        private bool isStreamOk(FileStream fs)
        {
            return (
                fs != null &&
                fs.Length == LEVEL_SIZE
                );
        }
    }
}