﻿using System.Collections.Generic;
using System.Linq;

namespace Supaplexer
{
    internal class ActorMap
    {
        public static Dictionary<byte, Actor> ByteToActor = new Dictionary<byte, Actor>()
        {
            { 0x00, new Actor("Empty Space", ImageStorage.ActorType.Void) },
            { 0x01, new Actor("Zonk", ImageStorage.ActorType.Zonk) },
            { 0x02, new Actor("Green Field", ImageStorage.ActorType.Green) },
            { 0x03, new Actor("Murphy (start)", ImageStorage.ActorType.Murphy) },
            { 0x04, new Actor("Infotron", ImageStorage.ActorType.Infotron) },
            { 0x05, new Actor("Chip", ImageStorage.ActorType.Chip) },
            { 0x06, new Actor("Grey Wall", ImageStorage.ActorType.GreyWall) },
            { 0x07, new Actor("Exit", ImageStorage.ActorType.Exit) },
            { 0x08, new Actor("Disk (falling)", ImageStorage.ActorType.BrownDisk) },
            { 0x09, new Actor("Port (East)", ImageStorage.ActorType.PortEast) },
            { 0x0A, new Actor("Port (South)", ImageStorage.ActorType.PortSouth) },
            { 0x0B, new Actor("Port (West)", ImageStorage.ActorType.PortWest) },
            { 0x0C, new Actor("Port (North)", ImageStorage.ActorType.PortNorth) },
            { 0x0D, new Actor("Special (East)", ImageStorage.ActorType.GravEast) },
            { 0x0E, new Actor("Special (South)", ImageStorage.ActorType.GravSouth) },
            { 0x0F, new Actor("Special (West)", ImageStorage.ActorType.GravWest) },
            { 0x10, new Actor("Special (North)", ImageStorage.ActorType.GravNorth) },
            { 0x11, new Actor("Snick Snack", ImageStorage.ActorType.SnickSnack) },
            { 0x12, new Actor("Disk (detonates)", ImageStorage.ActorType.YellowDisk) },
            { 0x13, new Actor("Terminal", ImageStorage.ActorType.Terminal) },
            { 0x14, new Actor("Disk (carryable)", ImageStorage.ActorType.RedDisk) },
            { 0x15, new Actor("Port (Vertical)", ImageStorage.ActorType.PortVertical) },
            { 0x16, new Actor("Port (Horizontal)", ImageStorage.ActorType.PortHorizontal) },
            { 0x17, new Actor("Port (4 way)", ImageStorage.ActorType.Port4Way) },
            { 0x18, new Actor("Electron", ImageStorage.ActorType.Electron) },
            { 0x19, new Actor("Bug", ImageStorage.ActorType.Bug) },
            { 0x1A, new Actor("Chip (left)", ImageStorage.ActorType.ChipLeft) },
            { 0x1B, new Actor("Chip (right)", ImageStorage.ActorType.ChipRight) },
            { 0x1C, new Actor("Wall (blue capacitor)", ImageStorage.ActorType.Fancy3) },
            { 0x1D, new Actor("Wall (green LED)", ImageStorage.ActorType.DiodeGreen) },
            { 0x1E, new Actor("Wall (blue LED)", ImageStorage.ActorType.DiodeBlue) },
            { 0x1F, new Actor("Wall (red LED)", ImageStorage.ActorType.DiodeRed) },
            { 0x20, new Actor("Wall (danger)", ImageStorage.ActorType.DangerWall) },
            { 0x21, new Actor("Wall (resistors)", ImageStorage.ActorType.Fancy1) },
            { 0x22, new Actor("Wall (red capacitor)", ImageStorage.ActorType.Fancy6) },
            { 0x23, new Actor("Wall (3 resistors)", ImageStorage.ActorType.Fancy5) },
            { 0x24, new Actor("Wall (red resistors)", ImageStorage.ActorType.Fancy4) },
            { 0x25, new Actor("Wall (yellow resistors)", ImageStorage.ActorType.Fancy2) },
            { 0x26, new Actor("Chip (top)", ImageStorage.ActorType.ChipTop) },
            { 0x27, new Actor("Chip (bottom)", ImageStorage.ActorType.ChipBottom) },

            // editor specific
            { 0xFF, new Actor("Invisible Wall", ImageStorage.ActorType.Invisible) }
        };

        public static byte ActorToByte(Actor actor)
        {
            return ActorMap.ByteToActor.First(p => p.Value.Equals(actor)).Key;
        }

        public static bool IsSpecialPort(Actor actor)
        {
            byte actorByte = ActorToByte(actor);

            return actorByte == 0x0D
                || actorByte == 0x0E
                || actorByte == 0x0F
                || actorByte == 0x10;
        }
    }
}