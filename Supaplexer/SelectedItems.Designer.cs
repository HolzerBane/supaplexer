﻿namespace Supaplexer
{
    partial class SelectedItems
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.primaryItem = new System.Windows.Forms.PictureBox();
            this.secondaryItem = new System.Windows.Forms.PictureBox();
            this.swapButton = new System.Windows.Forms.Button();
            this.swapToolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.primaryItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondaryItem)).BeginInit();
            this.SuspendLayout();
            // 
            // primaryItem
            // 
            this.primaryItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.primaryItem.Location = new System.Drawing.Point(4, 4);
            this.primaryItem.Name = "primaryItem";
            this.primaryItem.Size = new System.Drawing.Size(34, 34);
            this.primaryItem.TabIndex = 0;
            this.primaryItem.TabStop = false;
            // 
            // secondaryItem
            // 
            this.secondaryItem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.secondaryItem.Location = new System.Drawing.Point(37, 37);
            this.secondaryItem.Name = "secondaryItem";
            this.secondaryItem.Size = new System.Drawing.Size(34, 34);
            this.secondaryItem.TabIndex = 1;
            this.secondaryItem.TabStop = false;
            // 
            // swapButton
            // 
            this.swapButton.FlatAppearance.BorderSize = 0;
            this.swapButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.swapButton.Image = global::Supaplexer.Properties.Resources.swap_corner;
            this.swapButton.Location = new System.Drawing.Point(43, 4);
            this.swapButton.Margin = new System.Windows.Forms.Padding(1);
            this.swapButton.Name = "swapButton";
            this.swapButton.Size = new System.Drawing.Size(26, 26);
            this.swapButton.TabIndex = 2;
            this.swapToolTip.SetToolTip(this.swapButton, "Swap items");
            this.swapButton.UseVisualStyleBackColor = true;
            this.swapButton.Click += new System.EventHandler(this.swapButton_Click);
            // 
            // SelectedItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.swapButton);
            this.Controls.Add(this.primaryItem);
            this.Controls.Add(this.secondaryItem);
            this.Name = "SelectedItems";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.Size = new System.Drawing.Size(72, 72);
            ((System.ComponentModel.ISupportInitialize)(this.primaryItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.secondaryItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox primaryItem;
        private System.Windows.Forms.PictureBox secondaryItem;
        private System.Windows.Forms.Button swapButton;
        private System.Windows.Forms.ToolTip swapToolTip;
    }
}
