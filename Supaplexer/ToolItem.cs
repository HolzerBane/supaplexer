﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Supaplexer
{
    public partial class ToolItem : UserControl
    {
        private Actor m_actor;
        private Color m_defaultColor;

        public ToolItem()
        { }

        public ToolItem(Image image, Actor actor)
        {
            m_actor = actor;
            InitializeComponent();

            toolImage.Image = image;
            toolText.Text = actor.ToString();
            m_defaultColor = BackColor;
        }

        public void unselect()
        {
            BackColor = m_defaultColor;
        }

        public Actor getActor()
        {
            return m_actor;
        }

        private void ToolItem_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            Control control = sender as Control;
            Control cp = control.Parent;
            if (!(cp is ToolPanel)) cp = cp.Parent;
            (cp as ToolPanel).childPressed(this, me.Button);

            BackColor = Color.LightBlue;
        }
    }
}