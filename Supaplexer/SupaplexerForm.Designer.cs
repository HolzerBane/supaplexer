﻿namespace Supaplexer
{
    partial class SupaplexerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SupaplexerForm));
            this.mapPanel = new System.Windows.Forms.Panel();
            this.actorImages = new System.Windows.Forms.ImageList(this.components);
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadLevelFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.levelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chooseLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fillLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearLevelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infotronCountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MapToolboxSplit = new System.Windows.Forms.SplitContainer();
            this.MapLevelsTool = new System.Windows.Forms.SplitContainer();
            this.levelList = new System.Windows.Forms.ListView();
            this.col_num = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.col_name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LevelPropertiesLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.MainEditorSplit = new System.Windows.Forms.SplitContainer();
            this.ToolItemPalette = new System.Windows.Forms.SplitContainer();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.gravityPortsBinding = new System.Windows.Forms.BindingSource(this.components);
            this.LevelBinding = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.statusLabel = new System.Windows.Forms.Label();
            this.loadLevelProgressBar = new System.Windows.Forms.ProgressBar();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MapToolboxSplit)).BeginInit();
            this.MapToolboxSplit.Panel1.SuspendLayout();
            this.MapToolboxSplit.Panel2.SuspendLayout();
            this.MapToolboxSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MapLevelsTool)).BeginInit();
            this.MapLevelsTool.Panel1.SuspendLayout();
            this.MapLevelsTool.Panel2.SuspendLayout();
            this.MapLevelsTool.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainEditorSplit)).BeginInit();
            this.MainEditorSplit.Panel1.SuspendLayout();
            this.MainEditorSplit.Panel2.SuspendLayout();
            this.MainEditorSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ToolItemPalette)).BeginInit();
            this.ToolItemPalette.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gravityPortsBinding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LevelBinding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // mapPanel
            // 
            this.mapPanel.AutoScroll = true;
            this.mapPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapPanel.Location = new System.Drawing.Point(0, 0);
            this.mapPanel.Name = "mapPanel";
            this.mapPanel.Size = new System.Drawing.Size(455, 370);
            this.mapPanel.TabIndex = 1;
            this.mapPanel.MouseEnter += new System.EventHandler(this.mapPanel_MouseEnter);
            this.mapPanel.MouseLeave += new System.EventHandler(this.mapPanel_MouseLeave);
            // 
            // actorImages
            // 
            this.actorImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("actorImages.ImageStream")));
            this.actorImages.TransparentColor = System.Drawing.Color.Transparent;
            this.actorImages.Images.SetKeyName(0, "s_bug.jpg");
            this.actorImages.Images.SetKeyName(1, "s_chip.jpg");
            this.actorImages.Images.SetKeyName(2, "s_chipb.jpg");
            this.actorImages.Images.SetKeyName(3, "s_chipl.jpg");
            this.actorImages.Images.SetKeyName(4, "s_chipr.jpg");
            this.actorImages.Images.SetKeyName(5, "s_chipT.jpg");
            this.actorImages.Images.SetKeyName(6, "s_diodeb.jpg");
            this.actorImages.Images.SetKeyName(7, "s_diodeg.jpg");
            this.actorImages.Images.SetKeyName(8, "s_dioder.jpg");
            this.actorImages.Images.SetKeyName(9, "s_diskb.jpg");
            this.actorImages.Images.SetKeyName(10, "s_disky.jpg");
            this.actorImages.Images.SetKeyName(11, "s_exit.jpg");
            this.actorImages.Images.SetKeyName(12, "s_gwall.jpg");
            this.actorImages.Images.SetKeyName(13, "s_invis.jpg");
            this.actorImages.Images.SetKeyName(14, "s_resistor.jpg");
            this.actorImages.Images.SetKeyName(15, "s_rock.jpg");
            this.actorImages.Images.SetKeyName(16, "s_snick.jpg");
            this.actorImages.Images.SetKeyName(17, "s_start.jpg");
            this.actorImages.Images.SetKeyName(18, "s_terminal.jpg");
            this.actorImages.Images.SetKeyName(19, "s_unknown.jpg");
            this.actorImages.Images.SetKeyName(20, "s_void.jpg");
            this.actorImages.Images.SetKeyName(21, "s_walldanger.jpg");
            this.actorImages.Images.SetKeyName(22, "s_wallg.jpg");
            this.actorImages.Images.SetKeyName(23, "s_green.jpg");
            this.actorImages.Images.SetKeyName(24, "s_info.jpg");
            this.actorImages.Images.SetKeyName(25, "s_diskr.jpg");
            this.actorImages.Images.SetKeyName(26, "s_portE.jpg");
            this.actorImages.Images.SetKeyName(27, "s_portN.jpg");
            this.actorImages.Images.SetKeyName(28, "s_portS.jpg");
            this.actorImages.Images.SetKeyName(29, "s_portW.jpg");
            this.actorImages.Images.SetKeyName(30, "s_gravE.png");
            this.actorImages.Images.SetKeyName(31, "s_gravW.png");
            this.actorImages.Images.SetKeyName(32, "s_gravN.png");
            this.actorImages.Images.SetKeyName(33, "s_gravS.png");
            this.actorImages.Images.SetKeyName(34, "s_electron.jpg");
            this.actorImages.Images.SetKeyName(35, "s_port4.jpg");
            this.actorImages.Images.SetKeyName(36, "s_portH.jpg");
            this.actorImages.Images.SetKeyName(37, "s_portV.jpg");
            this.actorImages.Images.SetKeyName(38, "s_grav4.jpg");
            this.actorImages.Images.SetKeyName(39, "s_fancy1.jpg");
            this.actorImages.Images.SetKeyName(40, "s_fancy2.jpg");
            this.actorImages.Images.SetKeyName(41, "s_fancy3.jpg");
            this.actorImages.Images.SetKeyName(42, "s_fancy4.jpg");
            this.actorImages.Images.SetKeyName(43, "s_fancy5.jpg");
            this.actorImages.Images.SetKeyName(44, "s_fancy6.jpg");
            this.actorImages.Images.SetKeyName(45, "s_murphy.jpg");
            this.actorImages.Images.SetKeyName(46, "on.png");
            this.actorImages.Images.SetKeyName(47, "off.png");
            this.actorImages.Images.SetKeyName(48, "error.png");
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.levelToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(909, 24);
            this.mainMenu.TabIndex = 0;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadLevelFileToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadLevelFileToolStripMenuItem
            // 
            this.loadLevelFileToolStripMenuItem.Name = "loadLevelFileToolStripMenuItem";
            this.loadLevelFileToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.loadLevelFileToolStripMenuItem.Text = "Load level file";
            this.loadLevelFileToolStripMenuItem.Click += new System.EventHandler(this.loadLevelFileToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // levelToolStripMenuItem
            // 
            this.levelToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chooseLevelToolStripMenuItem,
            this.copyLevelToolStripMenuItem,
            this.fillLevelToolStripMenuItem,
            this.clearLevelToolStripMenuItem,
            this.infotronCountToolStripMenuItem});
            this.levelToolStripMenuItem.Name = "levelToolStripMenuItem";
            this.levelToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.levelToolStripMenuItem.Text = "Level";
            // 
            // chooseLevelToolStripMenuItem
            // 
            this.chooseLevelToolStripMenuItem.Name = "chooseLevelToolStripMenuItem";
            this.chooseLevelToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.chooseLevelToolStripMenuItem.Text = "Choose level";
            this.chooseLevelToolStripMenuItem.Click += new System.EventHandler(this.chooseLevelToolStripMenuItem_Click);
            // 
            // copyLevelToolStripMenuItem
            // 
            this.copyLevelToolStripMenuItem.Name = "copyLevelToolStripMenuItem";
            this.copyLevelToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.copyLevelToolStripMenuItem.Text = "Copy level";
            this.copyLevelToolStripMenuItem.Click += new System.EventHandler(this.copyLevelToolStripMenuItem_Click);
            // 
            // fillLevelToolStripMenuItem
            // 
            this.fillLevelToolStripMenuItem.Name = "fillLevelToolStripMenuItem";
            this.fillLevelToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.fillLevelToolStripMenuItem.Text = "Fill level";
            this.fillLevelToolStripMenuItem.Click += new System.EventHandler(this.fillLevelToolStripMenuItem_Click);
            // 
            // clearLevelToolStripMenuItem
            // 
            this.clearLevelToolStripMenuItem.Name = "clearLevelToolStripMenuItem";
            this.clearLevelToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.clearLevelToolStripMenuItem.Text = "Clear level";
            this.clearLevelToolStripMenuItem.Click += new System.EventHandler(this.clearLevelToolStripMenuItem_Click);
            // 
            // infotronCountToolStripMenuItem
            // 
            this.infotronCountToolStripMenuItem.Name = "infotronCountToolStripMenuItem";
            this.infotronCountToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.infotronCountToolStripMenuItem.Text = "Infotron count";
            // 
            // MapToolboxSplit
            // 
            this.MapToolboxSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MapToolboxSplit.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.MapToolboxSplit.IsSplitterFixed = true;
            this.MapToolboxSplit.Location = new System.Drawing.Point(0, 0);
            this.MapToolboxSplit.Name = "MapToolboxSplit";
            // 
            // MapToolboxSplit.Panel1
            // 
            this.MapToolboxSplit.Panel1.Controls.Add(this.MapLevelsTool);
            // 
            // MapToolboxSplit.Panel2
            // 
            this.MapToolboxSplit.Panel2.Controls.Add(this.LevelPropertiesLayout);
            this.MapToolboxSplit.Size = new System.Drawing.Size(909, 404);
            this.MapToolboxSplit.SplitterDistance = 685;
            this.MapToolboxSplit.TabIndex = 0;
            // 
            // MapLevelsTool
            // 
            this.MapLevelsTool.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MapLevelsTool.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.MapLevelsTool.Location = new System.Drawing.Point(0, 0);
            this.MapLevelsTool.Name = "MapLevelsTool";
            // 
            // MapLevelsTool.Panel1
            // 
            this.MapLevelsTool.Panel1.Controls.Add(this.levelList);
            // 
            // MapLevelsTool.Panel2
            // 
            this.MapLevelsTool.Panel2.Controls.Add(this.splitContainer2);
            this.MapLevelsTool.Panel2.MouseEnter += new System.EventHandler(this.MapLevelsTool_Panel2_MouseEnter);
            this.MapLevelsTool.Panel2.MouseLeave += new System.EventHandler(this.MapLevelsTool_Panel2_MouseLeave);
            this.MapLevelsTool.Size = new System.Drawing.Size(685, 404);
            this.MapLevelsTool.SplitterDistance = 226;
            this.MapLevelsTool.TabIndex = 1;
            // 
            // levelList
            // 
            this.levelList.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.levelList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.levelList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.col_num,
            this.col_name});
            this.levelList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.levelList.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.levelList.FullRowSelect = true;
            this.levelList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.levelList.HideSelection = false;
            this.levelList.Location = new System.Drawing.Point(0, 0);
            this.levelList.MultiSelect = false;
            this.levelList.Name = "levelList";
            this.levelList.Size = new System.Drawing.Size(226, 404);
            this.levelList.TabIndex = 0;
            this.levelList.UseCompatibleStateImageBehavior = false;
            this.levelList.View = System.Windows.Forms.View.Details;
            this.levelList.ItemActivate += new System.EventHandler(this.levelList_ItemActivate);
            this.levelList.SelectedIndexChanged += new System.EventHandler(this.levelList_SelectedIndexChanged);
            // 
            // col_num
            // 
            this.col_num.Text = "#";
            this.col_num.Width = 30;
            // 
            // col_name
            // 
            this.col_name.Text = "Name";
            this.col_name.Width = 192;
            // 
            // LevelPropertiesLayout
            // 
            this.LevelPropertiesLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LevelPropertiesLayout.Location = new System.Drawing.Point(0, 0);
            this.LevelPropertiesLayout.Name = "LevelPropertiesLayout";
            this.LevelPropertiesLayout.Size = new System.Drawing.Size(220, 404);
            this.LevelPropertiesLayout.TabIndex = 0;
            // 
            // MainEditorSplit
            // 
            this.MainEditorSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainEditorSplit.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.MainEditorSplit.IsSplitterFixed = true;
            this.MainEditorSplit.Location = new System.Drawing.Point(0, 0);
            this.MainEditorSplit.Name = "MainEditorSplit";
            this.MainEditorSplit.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // MainEditorSplit.Panel1
            // 
            this.MainEditorSplit.Panel1.Controls.Add(this.ToolItemPalette);
            this.MainEditorSplit.Panel1MinSize = 48;
            // 
            // MainEditorSplit.Panel2
            // 
            this.MainEditorSplit.Panel2.Controls.Add(this.MapToolboxSplit);
            this.MainEditorSplit.Size = new System.Drawing.Size(909, 504);
            this.MainEditorSplit.SplitterDistance = 96;
            this.MainEditorSplit.TabIndex = 1;
            // 
            // ToolItemPalette
            // 
            this.ToolItemPalette.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ToolItemPalette.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.ToolItemPalette.IsSplitterFixed = true;
            this.ToolItemPalette.Location = new System.Drawing.Point(0, 0);
            this.ToolItemPalette.Name = "ToolItemPalette";
            // 
            // ToolItemPalette.Panel2
            // 
            this.ToolItemPalette.Panel2.AutoScroll = true;
            this.ToolItemPalette.Size = new System.Drawing.Size(909, 96);
            this.ToolItemPalette.SplitterDistance = 84;
            this.ToolItemPalette.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.mainMenu);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.MainEditorSplit);
            this.splitContainer1.Size = new System.Drawing.Size(909, 533);
            this.splitContainer1.SplitterDistance = 25;
            this.splitContainer1.TabIndex = 0;
            // 
            // LevelBinding
            // 
            this.LevelBinding.DataSource = typeof(Supaplexer.Level);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.mapPanel);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.loadLevelProgressBar);
            this.splitContainer2.Panel2.Controls.Add(this.statusLabel);
            this.splitContainer2.Size = new System.Drawing.Size(455, 404);
            this.splitContainer2.SplitterDistance = 370;
            this.splitContainer2.TabIndex = 0;
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(3, 8);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(66, 13);
            this.statusLabel.TabIndex = 1;
            this.statusLabel.Text = "[status label]";
            // 
            // loadLevelProgressBar
            // 
            this.loadLevelProgressBar.Location = new System.Drawing.Point(137, 3);
            this.loadLevelProgressBar.Name = "loadLevelProgressBar";
            this.loadLevelProgressBar.Size = new System.Drawing.Size(218, 23);
            this.loadLevelProgressBar.TabIndex = 2;
            this.loadLevelProgressBar.Visible = false;
            // 
            // SupaplexerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 533);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenu;
            this.Name = "SupaplexerForm";
            this.Text = "Supaplex Editor Csoda";
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.MapToolboxSplit.Panel1.ResumeLayout(false);
            this.MapToolboxSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MapToolboxSplit)).EndInit();
            this.MapToolboxSplit.ResumeLayout(false);
            this.MapLevelsTool.Panel1.ResumeLayout(false);
            this.MapLevelsTool.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MapLevelsTool)).EndInit();
            this.MapLevelsTool.ResumeLayout(false);
            this.MainEditorSplit.Panel1.ResumeLayout(false);
            this.MainEditorSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainEditorSplit)).EndInit();
            this.MainEditorSplit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ToolItemPalette)).EndInit();
            this.ToolItemPalette.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gravityPortsBinding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LevelBinding)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private LevelView levelView;
        private System.Windows.Forms.Panel mapPanel;
        private System.Windows.Forms.ImageList actorImages;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadLevelFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem levelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chooseLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fillLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearLevelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infotronCountToolStripMenuItem;
        private System.Windows.Forms.SplitContainer MapToolboxSplit;
        private System.Windows.Forms.SplitContainer MapLevelsTool;
        private System.Windows.Forms.ListView levelList;
        private System.Windows.Forms.ColumnHeader col_num;
        private System.Windows.Forms.ColumnHeader col_name;
        private System.Windows.Forms.BindingSource LevelBinding;
        private System.Windows.Forms.SplitContainer MainEditorSplit;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer ToolItemPalette;
        private System.Windows.Forms.FlowLayoutPanel LevelPropertiesLayout;
        private System.Windows.Forms.BindingSource gravityPortsBinding;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ProgressBar loadLevelProgressBar;
        private System.Windows.Forms.Label statusLabel;
    }
}

