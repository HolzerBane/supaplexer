﻿namespace Supaplexer
{
    partial class SpecialPortProperties
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gravityPortList = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.freezeEnemiesCheck = new System.Windows.Forms.CheckBox();
            this.freezeZonksCheck = new System.Windows.Forms.CheckBox();
            this.enableGravityCheck = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gravityPortList
            // 
            this.gravityPortList.FormattingEnabled = true;
            this.gravityPortList.Location = new System.Drawing.Point(6, 19);
            this.gravityPortList.Name = "gravityPortList";
            this.gravityPortList.Size = new System.Drawing.Size(91, 160);
            this.gravityPortList.TabIndex = 0;
            this.gravityPortList.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.freezeEnemiesCheck);
            this.groupBox1.Controls.Add(this.freezeZonksCheck);
            this.groupBox1.Controls.Add(this.enableGravityCheck);
            this.groupBox1.Controls.Add(this.gravityPortList);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(206, 186);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Special ports";
            // 
            // freezeEnemiesCheck
            // 
            this.freezeEnemiesCheck.AutoSize = true;
            this.freezeEnemiesCheck.Location = new System.Drawing.Point(103, 67);
            this.freezeEnemiesCheck.Name = "freezeEnemiesCheck";
            this.freezeEnemiesCheck.Size = new System.Drawing.Size(100, 17);
            this.freezeEnemiesCheck.TabIndex = 3;
            this.freezeEnemiesCheck.Text = "Freeze enemies";
            this.freezeEnemiesCheck.UseVisualStyleBackColor = true;
            this.freezeEnemiesCheck.CheckedChanged += new System.EventHandler(this.freezeEnemiesCheck_CheckedChanged);
            // 
            // freezeZonksCheck
            // 
            this.freezeZonksCheck.AutoSize = true;
            this.freezeZonksCheck.Location = new System.Drawing.Point(103, 43);
            this.freezeZonksCheck.Name = "freezeZonksCheck";
            this.freezeZonksCheck.Size = new System.Drawing.Size(91, 17);
            this.freezeZonksCheck.TabIndex = 2;
            this.freezeZonksCheck.Text = "Freeze Zonks";
            this.freezeZonksCheck.UseVisualStyleBackColor = true;
            this.freezeZonksCheck.CheckedChanged += new System.EventHandler(this.freezeZonksCheck_CheckedChanged);
            // 
            // enableGravityCheck
            // 
            this.enableGravityCheck.AutoSize = true;
            this.enableGravityCheck.Location = new System.Drawing.Point(103, 19);
            this.enableGravityCheck.Name = "enableGravityCheck";
            this.enableGravityCheck.Size = new System.Drawing.Size(93, 17);
            this.enableGravityCheck.TabIndex = 1;
            this.enableGravityCheck.Text = "Enable gravity";
            this.enableGravityCheck.UseVisualStyleBackColor = true;
            this.enableGravityCheck.CheckedChanged += new System.EventHandler(this.enableGravityCheck_CheckedChanged);
            // 
            // GravityPortProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "GravityPortProperties";
            this.Size = new System.Drawing.Size(212, 193);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox gravityPortList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox enableGravityCheck;
        private System.Windows.Forms.CheckBox freezeEnemiesCheck;
        private System.Windows.Forms.CheckBox freezeZonksCheck;
    }
}
