﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Supaplexer
{
    /// <summary>
    /// A cell based interactive canvas
    /// </summary>
    public partial class LevelView : UserControl
    {
        private const int cellSide = 20;
        private bool levelDrawn = false;
        private Bitmap m_canvas = new Bitmap((int)Level.MAP_X * cellSide, (int)Level.MAP_Y * cellSide);
        private Bitmap m_frame = new Bitmap(cellSide, cellSide);
        private Bitmap m_highlight = new Bitmap(cellSide, cellSide);
        private PictureBox m_frameBox = new PictureBox();
        private PictureBox m_highlightBox = new PictureBox();
        private ImageList m_images;
        private Point lastPanCell;
        private Point lastMoveCell;
        private Point? lastPaintedCell;
        private Panel m_parent;
        private long scrollCalled;

        private Level m_level;
        private Actor m_selectedPriActor;
        private Actor m_selectedSecActor;
        private bool leftPressed = false;
        private bool rightPressed = false;
        private Point rightRelative;

        public delegate void levelLoadingHandle(int percentage);
        public delegate void levelLoadedHandle();
        public delegate void levelLoadingStartedHandle();
        public delegate void currentCellChanged(Point cell, Actor actor);
        public delegate void reportStatus(string message);

        public event levelLoadingHandle OnLevelLoading;
        public event levelLoadedHandle OnLevelLoaded;
        public event levelLoadingStartedHandle OnLevelLoadingStarted;
        public event currentCellChanged OnCurrentCellChanged;

        private Dictionary<MouseButtons, Action> mouseHandler = new Dictionary<MouseButtons, Action>();

        private Dictionary<Level, Bitmap> levelBitmapCache = new Dictionary<Level, Bitmap>();

        public event MouseEventHandler OnLevelViewClick
        {
            add { m_frameBox.MouseClick += value; }
            remove { m_frameBox.MouseClick -= value; }
        }

        public LevelView()
        {
            InitializeComponent();

            mouseHandler[MouseButtons.Left] = paintPriActorAtCursor;
            mouseHandler[MouseButtons.Right] = paintSecActorAtCursor;

            Controls.Add(m_highlightBox);
            Controls.Add(m_frameBox);


            m_highlightBox.Parent = mapImage;
            m_highlightBox.Width = cellSide;
            m_highlightBox.Height = cellSide;
            m_highlightBox.MouseMove += highlightBox_MouseMove;
            m_highlightBox.BackColor = Color.FromArgb(0, 0, 0, 0);

            m_frameBox.Parent = mapImage;
            m_frameBox.Width = cellSide;
            m_frameBox.Height = cellSide;
            m_frameBox.MouseDown += mapImage_MouseDown;
            m_frameBox.MouseClick += mapImage_Click;
            m_frameBox.MouseUp += mapImage_MouseUp;
            m_frameBox.MouseMove += mapImage_MouseMove;
            m_frameBox.BackColor = Color.FromArgb(0, 0, 0, 0);

            levelLoader.WorkerReportsProgress = true;
        }

        public void OnTick()
        {
            buildHighlight();
        }

        #region bindings

        public void bindPanel(Panel panel)
        {
            panel.Controls.Add(this);

            m_parent = panel;

            m_parent.HorizontalScroll.Maximum = (int)Level.MAP_X;
            m_parent.HorizontalScroll.Minimum = 1;

            m_parent.VerticalScroll.Maximum = (int)Level.MAP_Y;
            m_parent.VerticalScroll.Minimum = 1;
        }

        public void bindLevel(Level level)
        {
            if (m_level != null)
            {
                levelBitmapCache[m_level] = new Bitmap(m_canvas);
            }

            m_level = level;
        }

        public void bindImages(ImageList il)
        {
            m_images = il;
        }

        public void bindPriActor(Actor actor)
        {
            m_selectedPriActor = actor;
        }

        public void bindSecActor(Actor actor)
        {
            m_selectedSecActor = actor;
        }

        #endregion

        public Actor getActorAtCell(Point cell)
        {
            return m_level.getActor(cell.X, cell.Y);
        }

        public Level getLevel()
        {
            return m_level;
        }

        public void clearView()
        {
            using (Graphics grfx = Graphics.FromImage(m_canvas))
            {
                grfx.Clear(Color.Black);
            }

            using (Graphics grfx = Graphics.FromImage(m_frame))
            {
                grfx.Clear(Color.Black);
            }
            m_frameBox.Hide();
            m_highlightBox.Hide();
            levelDrawn = false;

            mapImage.Image = m_canvas;
            mapImage.Enabled = false;
        }

        public bool isLevelDrawn()
        {
            return levelDrawn;
        }

        #region draw

        /// <summary>
        /// Draws a rectangular grid between the cells
        /// </summary>
        public void drawGrid()
        {
            GraphicsUnit pix = GraphicsUnit.Pixel;

            using (Graphics grfx = Graphics.FromImage(m_canvas))
            {
                // vertical
                for (int x = 0; x <= Level.MAP_X; ++x)
                {
                    grfx.DrawLine(
                    new Pen(Color.Gray),
                    new Point(x * cellSide + cellSide - 1, cellSide),
                    new Point(x * cellSide + cellSide - 1, (int)(m_canvas.GetBounds(ref pix).Height))
                    );
                }

                // horizontal
                for (int y = 0; y <= Level.MAP_Y; ++y)
                {
                    grfx.DrawLine(
                        new Pen(Color.Gray),
                        new Point(cellSide, y * cellSide + cellSide - 1),
                        new Point((int)(m_canvas.GetBounds(ref pix).Width), y * cellSide + cellSide - 1)
                        );
                }
            }

            mapImage.Image = m_canvas;
        }  

        /// <summary>
        /// Draw all actors on the level
        /// </summary>
        public void drawLevel()
        {
            if (m_level == null) return;

            OnLevelLoadingStarted();

            clearView();

            if (levelBitmapCache.ContainsKey(m_level))
            {
                m_canvas = new Bitmap(levelBitmapCache[m_level]);
                finaliseDrawLevel();
            }
            else
            {
                levelLoader.RunWorkerAsync();
            }

        }

        #endregion

        #region grid helpers

        private Point bottomLeftofCell(int x, int y)
        {
            Point blVertex = new Point();

            blVertex.X = x * cellSide;
            blVertex.Y = y * cellSide;// +cellSide;

            return blVertex;
        }

        private Point bottomLeftofCell(Point point)
        {
            return bottomLeftofCell(point.X, point.Y);
        }

        private Point getCell(int offset)
        {
            Point cellIndex = new Point();

            cellIndex.X = offset % ((int)Level.MAP_X);
            cellIndex.Y = offset / (int)Level.MAP_X;

            return cellIndex;
        }

        private Point getCell(int x, int y)
        {
            Point cellIndex = new Point();
            cellIndex.X = x / (int)cellSide;
            cellIndex.Y = y / (int)cellSide;

            return cellIndex;
        }

        private Point getCell(Point mousePos)
        {
            return getCell(mousePos.X, mousePos.Y);
        }

        private void drawAt(Graphics grfx, int x, int y, Image image, int w, int h)
        {
            grfx.DrawImage(image, new Rectangle(x, y, w, h));
        }

        /// <summary>
        /// Draw a frame on a cell
        /// </summary>
        /// <param name="cell">the cell to draw the frame on</param>
        private void drawFrame(Point cell)
        {
            if (!isCellValid(cell))
            {
                return;
            }

            Point bl = bottomLeftofCell(cell);

            m_frameBox.Location = bl;
            buildFrame();
            m_frameBox.BringToFront();
            m_frameBox.Show();
        }

        private bool isCellValid(Point cell)
        {
            return cell.X > 0 && cell.X < Level.MAP_X &&
                   cell.Y > 0 && cell.Y < Level.MAP_Y;
        }

        /// <summary>
        /// draw a frame on the given image
        /// </summary>
        /// <param name="back">image to draw the frame on</param>
        private void buildFrame()
        {
            using (Graphics grfx = Graphics.FromImage(m_frame))
            {
                int edge = cellSide - 1;
                grfx.Clear(Color.FromArgb(0, 0, 0, 0));

                Pen pen = new Pen(Color.Red);
                grfx.DrawLine(pen, 0, 0, 5, 0);
                grfx.DrawLine(pen, 0, 0, 0, 5);

                grfx.DrawLine(pen, 0, edge, 0, edge - 5);
                grfx.DrawLine(pen, 0, edge, 5, edge);

                grfx.DrawLine(pen, edge, 0, edge, 5);
                grfx.DrawLine(pen, edge, 0, edge - 5, 0);

                grfx.DrawLine(pen, edge, edge, edge, edge - 5);
                grfx.DrawLine(pen, edge, edge, edge - 5, edge);

                m_frameBox.Image = m_frame;
            }
        }

        private void buildHighlight()
        {
            using (Graphics grfx = Graphics.FromImage(m_highlight))
            {
                int edge = cellSide - 1;
                grfx.Clear(Color.FromArgb(0, 0, 0, 0));

                Pen pen = new Pen(Color.Yellow);
                int length = 3;
                pen.DashPattern = new float[2] { length, length };
                pen.DashOffset = (int)((DateTime.Now.Ticks / 1000000) % (length + 2));
                
                grfx.DrawLine(pen, 0, 0, cellSide, 0);
                grfx.DrawLine(pen, 0, cellSide, 0, 0);
                grfx.DrawLine(pen, edge, 0, edge, edge);
                grfx.DrawLine(pen, cellSide - 1, cellSide - 1, 0, cellSide - 1);

                m_highlightBox.Image = m_highlight;
            }
        }

        public void gravityPortSelected(SpecialPort sp)
        {
            updateCell(sp.Cell);
            highlightCell(sp.Cell);
        }

        public void highlightCell(Point cell)
        {
            Point bl = bottomLeftofCell(cell);

            m_highlightBox.Location = bl;
            buildHighlight();
            m_highlightBox.Show();
        }

        public Image getActorImage(Point cell)
        {
            Actor actor = getActorAtCell(cell);
            Image actImg = getActorImage(actor);

            if (actor.IsSpecialPort())
            {
                var sp = m_level.getGravityPort(cell);
                string overlay_key = "error.png";
                if (sp != null)
                {
                    overlay_key = sp.Gravity ? "on.png" : "off.png";
                }
               
                Image overlay = m_images.Images[overlay_key];
                using (Graphics actorGfx = Graphics.FromImage(actImg))
                {
                    drawAt(actorGfx, 0, 0, actImg, 32, 32);
                    drawAt(actorGfx, 0, 0, overlay, 32, 32);
                }
                
            }

            return actImg;
        }

        public Image getActorImage(Actor actor)
        {
            string key = ImageStorage.ActorImgToResEntry[actor.Image];
            return m_images.Images[key];
        }

        #endregion

        #region events

        private void mapImage_MouseHover(object sender, EventArgs e)
        {
        }

        private void highlightBox_MouseMove(object sender, MouseEventArgs e)
        {
            mapImage_MouseMove(sender, e);
        }

        private void mapImage_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_level == null) return;

            Point ptCursor = Cursor.Position;
            Point controlPos;
            controlPos = mapImage.PointToClient(ptCursor);
            var currentCell = getCell(controlPos);
            if (lastMoveCell != currentCell && isCellValid(currentCell))
            {
                OnCurrentCellChanged(currentCell, getActorAtCell(currentCell));
            }

            lastMoveCell = currentCell;
            drawFrame(currentCell);
            m_frameBox.BringToFront();

            if (leftPressed)
            {
                paintPriActorAtCursor();
            }

            if (rightPressed)
            {
                paintSecActorAtCursor();
                //panView();
            }
        }

        private void mapImage_MouseDown(object sender, MouseEventArgs e)
        {
            if (e == null) return;
            if (m_level == null) return;

            if (e.Button == MouseButtons.Left)
            {
                leftPressed = true;
            }

            if (e.Button == MouseButtons.Right)
            {
                rightPressed = true;
            }

            Point controlPos = mapImage.PointToClient(Cursor.Position);
            Point cell = getCell(controlPos);

            mouseHandler[e.Button]();
        }

        private void mapImage_Click(object sender, EventArgs e)
        {
            Point controlPos = mapImage.PointToClient(Cursor.Position);
        }

        private void LevelView_Click(object sender, EventArgs e)
        {
            mapImage_MouseDown(null, null);
        }

        private void mapImage_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                leftPressed = false;
                lastPaintedCell = null;
            }

            if (e.Button == MouseButtons.Right)
            {
                rightPressed = false;
            }

        }

        public void mouseEnteredParent(object sender, EventArgs e)
        {
            m_frameBox.Show();
        }

        public void mouseLeftParent(object sender, EventArgs e)
        {
            leftPressed = false;
            rightPressed = false;
            m_frameBox.Hide();
        }

        #endregion

        #region actions

        private void rightButtonPressed()
        {
            if (!rightPressed)
            {
                lastPanCell = getCell(mapImage.PointToClient(Cursor.Position));
            }

            rightPressed = true;
            rightRelative = mapImage.PointToClient(Cursor.Position);
        }

        private void paintPriActorAtCursor()
        {
            paintActorAtCursor(m_selectedPriActor);
        }

        private void paintSecActorAtCursor()
        {
            paintActorAtCursor(m_selectedSecActor);
        }

        private void paintActorAtCursor(Actor actor)
        {
            if (actor == null) return;

            Point controlPos = mapImage.PointToClient(Cursor.Position);
            Point cell = getCell(controlPos);

            if (!isCellValid(cell) || lastPaintedCell == cell)
            {
                return;
            }

            lastPaintedCell = cell;

            Actor prevActor = m_level.getActor(cell.X, cell.Y);

            // Get the gravity port
            var sp = m_level.getGravityPort(cell);

            // Actor to be painted is a gravity port
            if (actor.IsSpecialPort())
            {
                if (sp != null)
                {
                    // The occupying actor is a gravity port as well
                    if (actor.Image == prevActor.Image)
                    {
                        // Swap gravity state if the two actor types are the same
                        m_level.setGravity(sp, !sp.Gravity);

                    }
                }
                else
                {
                    // This is a new gravity port, add one
                    if (m_level.getSpecialPortCount() == Level.SPECIAL_PORT_COUNT)
                    {
                        var result = MessageBox.Show("You have already placed 10 gravity ports.\nA new one will overwrite the oldest!\nContinue?", "Question",
                                              MessageBoxButtons.YesNoCancel,
                                              MessageBoxIcon.Question);

                        if (result != DialogResult.Yes)
                        {
                            return;
                        }
                    }

                    m_level.addSpecialPort(cell);
                }
            }
            else if (sp != null)
            {
                m_level.removeSpecialPort(cell);
            }

            m_level.setActor(cell.X, cell.Y, actor);

            updateCell(cell);

        }

        private void updateCell(Point cell)
        {
            using (Graphics grfx = Graphics.FromImage(m_canvas))
            {
                Image actImg = getActorImage(cell);
                Point bl = bottomLeftofCell(cell);
                drawAt(grfx, bl.X, bl.Y, actImg, cellSide, cellSide);
            }
        }

        private void panView()
        {
            Point currentPoint = mapImage.PointToClient(Cursor.Position);
            Point currentCell = getCell(currentPoint);
            Point relative = lastPanCell;
            relative.X -= currentCell.X;
            relative.Y -= currentCell.Y;

            if (relative.X != 0 || relative.Y != 0)
            {
                scrollCalled = DateTime.Now.Ticks;

                int XScroll = 0;
                if (relative.X > 0) XScroll = 1;
                else if (relative.X < 0) XScroll = -1;

                int YScroll = 0;
                if (relative.Y > 0) YScroll = 1;
                else if (relative.Y < 0) YScroll = -1;

                int newX = -m_parent.AutoScrollPosition.X + XScroll * cellSide;
                int newY = -m_parent.AutoScrollPosition.Y + YScroll * cellSide;
                m_parent.AutoScrollPosition = new Point(newX, newY);
            }
        }


        #endregion

        private void mapImage_MouseEnter(object sender, EventArgs e)
        {
            mouseEnteredParent(sender, e);
        }

        private void mapImage_MouseLeave(object sender, EventArgs e)
        {
            mouseLeftParent(sender, e);

        }

        private void drawLevelTask()
        {
            using (Graphics grfx = Graphics.FromImage(m_canvas))
            {
                int progress = 0;
                for (int index = 0; index < m_level.getActors().Length; ++index)
                {
                    Point cell = getCell(index);
                    Point bl = bottomLeftofCell(cell);
                    Image actImg = getActorImage(cell);

                    drawAt(grfx, bl.X, bl.Y, actImg, cellSide, cellSide);

                    levelLoader.ReportProgress(progress++);
                }
            }
        }

        private void finaliseDrawLevel()
        {
            mapImage.Image = m_canvas;
            mapImage.Enabled = true;
            levelDrawn = true;

            OnLevelLoaded();
        }

        private void levelLoader_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            drawLevelTask();
        }

        private void levelLoader_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            OnLevelLoading(e.ProgressPercentage);
        }

        private void levelLoader_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            finaliseDrawLevel();
        }
    }
}