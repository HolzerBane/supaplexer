﻿using System.Collections.Generic;

namespace Supaplexer
{
    public class ImageStorage
    {
        public enum ActorType
        {
            Void,
            GreyWall,
            Green,
            Exit,
            Murphy,
            SnickSnack,
            Electron,
            Infotron,
            RedDisk,
            Bug,
            Zonk,
            BrownDisk,
            YellowDisk,
            Terminal,
            ChipLeft,
            ChipRight,
            ChipTop,
            ChipBottom,
            Chip,

            PortEast,
            PortWest,
            PortNorth,
            PortSouth,
            PortHorizontal,
            PortVertical,
            Port4Way,

            GravEast,
            GravWest,
            GravNorth,
            GravSouth,
            GravHorizontal,
            GravVertical,
            Grav4Way,

            DiodeRed,
            DiodeBlue,
            DiodeGreen,
            DangerWall,
            ResistorWall,

            Fancy1,
            Fancy2,
            Fancy3,
            Fancy4,
            Fancy5,
            Fancy6,

            Invisible,
            Unknown
        }

        public static Dictionary<ActorType, string> ActorImgToResEntry = new Dictionary<ActorType, string>()
        {
            { ActorType.Void, "s_void.jpg"},
            { ActorType.GreyWall, "s_wallg.jpg"},
            { ActorType.Exit, "s_exit.jpg"},
            { ActorType.Murphy, "s_murphy.jpg"},
            { ActorType.Infotron, "s_info.jpg"},
            { ActorType.Green, "s_green.jpg"},
            { ActorType.Invisible, "s_invis.jpg"},
            { ActorType.Unknown, "s_unknown.jpg"},
            { ActorType.Zonk, "s_rock.jpg"},
            { ActorType.Bug, "s_bug.jpg"},

            { ActorType.Chip, "s_chip.jpg"},
            { ActorType.ChipLeft, "s_chipl.jpg"},
            { ActorType.ChipRight, "s_chipr.jpg"},
            { ActorType.ChipTop, "s_chipt.jpg"},
            { ActorType.ChipBottom, "s_chipb.jpg"},

            { ActorType.DiodeBlue, "s_diodeb.jpg"},
            { ActorType.DiodeGreen, "s_diodeg.jpg"},
            { ActorType.DiodeRed, "s_dioder.jpg"},
            { ActorType.ResistorWall, "s_resistor.jpg"},
            { ActorType.DangerWall, "s_walldanger.jpg"},

            { ActorType.Fancy1, "s_fancy1.jpg"},
            { ActorType.Fancy2, "s_fancy2.jpg"},
            { ActorType.Fancy3, "s_fancy3.jpg"},
            { ActorType.Fancy4, "s_fancy4.jpg"},
            { ActorType.Fancy5, "s_fancy5.jpg"},
            { ActorType.Fancy6, "s_fancy6.jpg"},

            { ActorType.BrownDisk, "s_diskb.jpg"},
            { ActorType.YellowDisk, "s_disky.jpg"},
            { ActorType.RedDisk, "s_diskr.jpg"},

            { ActorType.SnickSnack, "s_snick.jpg"},
            { ActorType.Electron, "s_electron.jpg"},
            { ActorType.Terminal,  "s_terminal.jpg"},

            { ActorType.PortEast, "s_portE.jpg"},
            { ActorType.PortWest, "s_portW.jpg"},
            { ActorType.PortNorth, "s_portN.jpg"},
            { ActorType.PortSouth, "s_portS.jpg"},
            { ActorType.PortHorizontal, "s_portH.jpg"},
            { ActorType.PortVertical, "s_portV.jpg"},
            { ActorType.Port4Way, "s_port4.jpg"},

            { ActorType.GravEast, "s_gravE.png"},
            { ActorType.GravWest, "s_gravW.png"},
            { ActorType.GravNorth, "s_gravN.png"},
            { ActorType.GravSouth, "s_gravS.png"},
            { ActorType.GravHorizontal, "s_gravH.jpg"},
            { ActorType.GravVertical, "s_gravV.jpg"},
            { ActorType.Grav4Way, "s_grav4.jpg"},
        };

        // Categories
        public static List<ActorType> StartExit = new List<ActorType>()
        {
            ActorType.Murphy,
            ActorType.Exit
        };

        public static List<ActorType> Hazards = new List<ActorType>()
        {
            ActorType.SnickSnack,
            ActorType.Electron,
            ActorType.Bug,
            ActorType.Zonk,
            ActorType.BrownDisk
        };

        public static List<ActorType> Walls = new List<ActorType>()
        {
            ActorType.Void,
            ActorType.GreyWall,
            ActorType.Green
        };

        public static List<ActorType> Collectibles = new List<ActorType>()
        {
            ActorType.Infotron,
            ActorType.RedDisk,
            ActorType.Green
        };
    }
}